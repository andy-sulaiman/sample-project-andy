package application.test.transaction;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import application.backend.repository.transaction.TransactionRepository;
import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionSearchResponse;
import application.rest.transaction.TransactionController;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTests
{
    @Mock
    private TransactionRepository transactionRepository;

    @Test
    public void testTransactionSearchOne() throws Exception
    {
        List<Transaction> list = new ArrayList<Transaction>();
        Transaction response = new Transaction("999", null, null, new Date(), null, null, null, null, new Date(), null, null, null, false);
        list.add(response);
        
        TransactionController controller = new TransactionController(transactionRepository);
        Mockito.when(transactionRepository.findOne("10000")).thenReturn(response);
        
        List<Transaction> responseList = controller.transactionSearch("10000").getContent();

        assertEquals("Lists are not equal", list, responseList);
        Mockito.verify(transactionRepository).findOne("10000");
    }
    
    @Test
    public void testTransactionSearchOneNotExist() throws Exception
    {
        List<Transaction> list = new ArrayList<Transaction>();
        
        TransactionController controller = new TransactionController(transactionRepository);
        Mockito.when(transactionRepository.findOne("10000")).thenReturn(null);
        
        TransactionSearchResponse searchResponse = controller.transactionSearch("10000");
        List<Transaction> responseList = searchResponse.getContent();
        int count = searchResponse.getCount();

        assertEquals("Lists are not equal", list, responseList);
        assertEquals("Counts not equal", list.size(), count);
        Mockito.verify(transactionRepository).findOne("10000");
    }

    @Test
    public void testTransactionGetAll()
    {
        Transaction t1 = new Transaction("999", null, null, new Date(), null, null, null, null, new Date(), null, null, null, false);
        Transaction t2 = new Transaction("555", "Yes", null, new Date(), null, null, null, null, new Date(), null, null, null, false);
        
        List<Transaction> list = new ArrayList<Transaction>();
        list.add(t1);
        list.add(t2);

        TransactionController controller = new TransactionController(transactionRepository);
        Mockito.when(transactionRepository.findAll()).thenReturn(list);

        TransactionSearchResponse searchResponse = controller.allTransactions();
        List<Transaction> responseList = searchResponse.getContent();
        int count = searchResponse.getCount();

        assertEquals("Lists are not equal", list, responseList);
        assertEquals("Counts are not equal", list.size(), count);
        Mockito.verify(transactionRepository).findAll();
    }

    @Test
    public void testAddingTransaction()
    {
        TransactionController controller = new TransactionController(transactionRepository);

        controller.createTransaction(
                new Transaction("555", "Nope", null, new Date(), null, null, null, null, new Date(), null, null, null, false));

        ArgumentCaptor<Transaction> transactionArguments = ArgumentCaptor.forClass(Transaction.class);

        Mockito.verify(transactionRepository).save(transactionArguments.capture());
        assertEquals("555", transactionArguments.getValue().getTrxnId());
        assertEquals("Nope", transactionArguments.getValue().getNetId());
        assertEquals(null, transactionArguments.getValue().getNetName());
    }

    @Test
    public void testDeleteTransactionsById()
    {
        TransactionController controller = new TransactionController(transactionRepository);

        controller.deleteTransaction("555");

        Mockito.verify(transactionRepository).delete("555");
    }
}
