package application.test.transaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionSearchResponse;

public class TransactionTests
{    
    @Test
    public void testTransactionResponseHasCorrectCount()
    {        
        List<Transaction> list1 = new ArrayList<Transaction>();
        TransactionSearchResponse response1 = new TransactionSearchResponse(list1);
        assertEquals("response1 count incorrect", 0, response1.getCount());

        List<Transaction> list2 = new ArrayList<Transaction>();
        list2.add(null);
        list2.add(null);
        TransactionSearchResponse response2 = new TransactionSearchResponse(list2);
        assertEquals("response2 count incorrect", 2, response2.getCount());
    }

    @Test
    public void testTransactionResponseHasCorrectContent()
    {
        List<Transaction> list = new ArrayList<Transaction>();
        list.add(new Transaction("999", null, null, new Date(), null, null, null, null, new Date(), null, null, null, false));

        TransactionSearchResponse response = new TransactionSearchResponse(list);
        assertEquals(list, response.getContent());
        assertTrue(true);
    }
}
