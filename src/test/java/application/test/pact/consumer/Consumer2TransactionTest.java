package application.test.pact.consumer;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionCreateResponse;
import application.dto.transaction.TransactionSearchResponse;
import application.rest.RestConfig;
import au.com.dius.pact.consumer.ConsumerPactTest;
import au.com.dius.pact.consumer.dsl.PactDslResponse;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.PactFragment;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(classes={RestConfig.class})
public class Consumer2TransactionTest extends ConsumerPactTest
{
    @Autowired
    private RestTemplate template;
    
    @Override
    protected PactFragment createFragment(PactDslWithProvider builder)
    {
        PactDslResponse state1Response = setupState1Tests(builder);
        PactDslResponse state2Response = setupState2Tests(state1Response);
        
        return state2Response.toFragment();
    }

    @Override
    protected String providerName()
    {
        return "transactionsbabey";
    }

    @Override
    protected String consumerName()
    {
        return "consumer2";
    }

    @Override
    protected void runTest(String url) throws IOException
    {
        int getStatus = template.getForEntity(url + "/transaction", TransactionSearchResponse.class)
            .getStatusCode().value();
        
        template.getForEntity(url + "/transaction/10000", TransactionSearchResponse.class);
        
        Transaction t = new Transaction("10000", "123", "NETWORK", null, "1", "BTB", 
                "ELE", "NOUS", null, "DIPS", "UNPAID", "3", false);
        int postStatus = template.postForEntity(url + "/transaction/create", t, 
                TransactionCreateResponse.class)
                .getStatusCode().value();
        
        template.getForEntity(url + "/transaction/10002", TransactionSearchResponse.class);
        
        assertTrue(getStatus == 200);
        assertTrue(postStatus == 200);
    }
    
    private PactDslResponse setupState1Tests(PactDslWithProvider builder)
    {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        
        JSONObject validTransaction = new JSONObject();
        validTransaction.put("trxnId", "10000");
        validTransaction.put("netId", "123");
        validTransaction.put("netName", "NETWORK");
        validTransaction.put("dateReceived", JSONObject.NULL);
        validTransaction.put("retailerId", "1");
        validTransaction.put("transactionSource", "BTB");
        validTransaction.put("utilityType", "ELE");
        validTransaction.put("transactionType", "NOUS");
        validTransaction.put("transactionDate", JSONObject.NULL);
        validTransaction.put("transactionStatus", "DIPS");
        validTransaction.put("paymentStatus", "UNPAID");
        validTransaction.put("glId", "3");
        validTransaction.put("annotated", false);

        JSONObject response = createGetTransaction10000Response();
        
        return builder
                .given("state1")
                    .uponReceiving("get transaction request")
                        .path("/transaction")
                        .method("GET")
                    .willRespondWith()
                        .status(200)
                    .uponReceiving("get transacion 10000 request")
                        .path("/transaction/10000")
                        .method("GET")
                    .willRespondWith()
                        .status(200)
                        .body(response)
                    .uponReceiving("create transaction request")
                        .path("/transaction/create")
                        .method("POST")
                        .headers(headers)
                        .body(validTransaction)
                    .willRespondWith()
                        .status(200)
                        .body("{\"message\":\"Successfully added transaction with id: 10000\"}", 
                                "application/json");
    }
    
    private JSONObject createGetTransaction10000Response()
    {
        JSONObject content = new JSONObject();
        content.put("trxnId", "10000");
        
        JSONArray array = new JSONArray();
        array.put(content);
        
        JSONObject return10000Transaction = new JSONObject();
        return10000Transaction.put("count", 1);
        return10000Transaction.put("content", array);
        
        return return10000Transaction;
    }
    
    private PactDslResponse setupState2Tests(PactDslResponse responseChain)
    {
        JSONObject response = createGetTransaction10002Response();
        
        return responseChain
                .given("state2")
                    .uponReceiving("some request")
                        .path("/transaction/10002")
                        .method("GET")
                    .willRespondWith()
                        .status(200)
                        .body(response);
    }
    
    private JSONObject createGetTransaction10002Response()
    {
        JSONObject content = new JSONObject();
        content.put("trxnId", "10002");
        content.put("netName", "The Name");
        
        JSONArray array = new JSONArray();
        array.put(content);
        
        JSONObject return10000Transaction = new JSONObject();
        return10000Transaction.put("count", 1);
        return10000Transaction.put("content", array);
        
        return return10000Transaction;
    }
}
