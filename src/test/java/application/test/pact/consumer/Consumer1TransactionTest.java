package application.test.pact.consumer;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionCreateResponse;
import application.dto.transaction.TransactionSearchResponse;
import application.rest.RestConfig;
import au.com.dius.pact.consumer.ConsumerPactTest;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.PactFragment;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(classes={RestConfig.class})
public class Consumer1TransactionTest extends ConsumerPactTest
{
    @Autowired
    private RestTemplate template;
    
    @Override
    protected PactFragment createFragment(PactDslWithProvider builder)
    {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        
        JSONObject validTransaction = new JSONObject();
        validTransaction.put("trxnId", "10000");
        validTransaction.put("netId", "123");
        validTransaction.put("netName", "NETWORK");
        validTransaction.put("dateReceived", JSONObject.NULL);
        validTransaction.put("retailerId", "1");
        validTransaction.put("transactionSource", "BTB");
        validTransaction.put("utilityType", "ELE");
        validTransaction.put("transactionType", "NOUS");
        validTransaction.put("transactionDate", JSONObject.NULL);
        validTransaction.put("transactionStatus", "DIPS");
        validTransaction.put("paymentStatus", "UNPAID");
        validTransaction.put("glId", "3");
        validTransaction.put("annotated", false);
        
        return builder
                .uponReceiving("get transaction request")
                    .path("/transaction")
                    .method("GET")
                .willRespondWith()
                    .status(200)
                .uponReceiving("create transaction request")
                    .path("/transaction/create")
                    .method("POST")
                    .headers(headers)
                    .body(validTransaction)
                .willRespondWith()
                    .status(200)
                    .body("{\"message\":\"Successfully added transaction with id: 10000\"}", 
                            "application/json")
                .toFragment();
    }

    @Override
    protected String providerName()
    {
        return "transactionsbabey";
    }

    @Override
    protected String consumerName()
    {
        return "consumer1";
    }

    @Override
    protected void runTest(String url) throws IOException
    {
        int getStatus = template.getForEntity(url + "/transaction", TransactionSearchResponse.class)
            .getStatusCode().value();
        
        Transaction t = new Transaction("10000", "123", "NETWORK", null, "1", "BTB", 
                "ELE", "NOUS", null, "DIPS", "UNPAID", "3", false);
        int postStatus = template.postForEntity(url + "/transaction/create", t, 
                TransactionCreateResponse.class)
                .getStatusCode().value();
        
        assertTrue(getStatus == 200);
        assertTrue(postStatus == 200);
    }
}
