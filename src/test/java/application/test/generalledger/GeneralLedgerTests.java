package application.test.generalledger;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.dto.generalledger.GeneralLedger;
import application.rest.generalledger.GeneralLedgerController;

public class GeneralLedgerTests
{
    @Test
    public void testGettingGeneralLedger()
    {
        GeneralLedger ledger = new GeneralLedger("ABCD-ABCD-BABA", "ELE", "4", "Gl Default", "3365");

        GeneralLedgerController controller = new GeneralLedgerController();

        GeneralLedger returnLedger = controller.getGeneralLedger();

        assertEquals("The ledgers are not the same", ledger.getDescription(), returnLedger.getDescription());
        assertEquals("The ledgers are not the same", ledger.getGl_code(), returnLedger.getGl_code());
        assertEquals("The ledgers are not the same", ledger.getGl_id(), returnLedger.getGl_id());
        assertEquals("The ledgers are not the same", ledger.getLast_update_id(), returnLedger.getLast_update_id());
        assertEquals("The ledgers are not the same", ledger.getUtility_type(), returnLedger.getUtility_type());
    }
}
