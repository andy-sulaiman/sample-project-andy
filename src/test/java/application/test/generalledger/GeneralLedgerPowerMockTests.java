package application.test.generalledger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import application.dto.generalledger.GeneralLedger;
import application.rest.generalledger.GeneralLedgerController;
import application.util.generalledger.GeneralLedgerUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ GeneralLedgerUtil.class })
public class GeneralLedgerPowerMockTests
{
    @Test
    public void testGeneralLedgerClone()
    {
        GeneralLedger gl = new GeneralLedger("code", "utility", "id", "description", "last_updated");
        GeneralLedger other = new GeneralLedger("code", "utility", "id", "description", "last_updated");

        GeneralLedgerController controller = new GeneralLedgerController();

        PowerMockito.mockStatic(GeneralLedgerUtil.class);
        PowerMockito.when(GeneralLedgerUtil.cloneLedger(gl)).thenReturn(other);

        GeneralLedger cloned = controller.cloneAndReturn(gl);

        Assert.assertEquals(gl.getGl_code(), cloned.getGl_code());
        Assert.assertEquals(gl.getUtility_type(), cloned.getUtility_type());
        Assert.assertEquals(gl.getGl_id(), cloned.getGl_id());
        Assert.assertEquals(gl.getDescription(), cloned.getDescription());
        Assert.assertEquals(gl.getLast_update_id(), cloned.getLast_update_id());
        Assert.assertNotEquals("Variables referring to same object in memory!", gl, cloned);
    }
}
