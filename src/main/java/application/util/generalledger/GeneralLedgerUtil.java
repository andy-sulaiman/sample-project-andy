package application.util.generalledger;

import application.dto.generalledger.GeneralLedger;

public class GeneralLedgerUtil
{
    public static GeneralLedger cloneLedger(GeneralLedger orig)
    {
        return new GeneralLedger(orig.getGl_code(), orig.getUtility_type(), orig.getGl_id(), orig.getDescription(),
                orig.getLast_update_id());
    }
}
