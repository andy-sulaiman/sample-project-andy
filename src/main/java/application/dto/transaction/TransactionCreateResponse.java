package application.dto.transaction;

public class TransactionCreateResponse
{
    private String message;
    
    public TransactionCreateResponse()
    {}
    
    public TransactionCreateResponse(String message)
    {
        this.message = message;
    }
    
    public String getMessage()
    {
        return this.message;
    }
}
