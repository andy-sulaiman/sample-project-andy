package application.dto.transaction;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Transaction
{
    @Id
    private String trxnId;
    private String netId;
    private String netName;
    private Date dateReceived;
    private String retailerId;
    private String transactionSource;
    private String utilityType;
    private String transactionType;
    private Date transactionDate;
    private String transactionStatus;
    private String paymentStatus;
    private String glId;
    private boolean isAnnotated;

    public Transaction()
    {
    }

    public Transaction(String trxnId, String netId, String netName, Date dateReceived, String retailerId,
            String transactionSource, String utilityType, String transactionType, Date transactionDate,
            String transactionStatus, String paymentStatus, String glId, boolean isAnnotated)
    {
        super();
        this.trxnId = trxnId;
        this.netId = netId;
        this.netName = netName;
        this.dateReceived = dateReceived == null ? null : new Date(dateReceived.getTime());
        this.retailerId = retailerId;
        this.transactionSource = transactionSource;
        this.utilityType = utilityType;
        this.transactionType = transactionType;
        this.transactionDate = transactionDate == null ? null : new Date(transactionDate.getTime());
        this.transactionStatus = transactionStatus;
        this.paymentStatus = paymentStatus;
        this.glId = glId;
        this.isAnnotated = isAnnotated;
    }

    public String getTrxnId()
    {
        return trxnId;
    }

    public String getNetId()
    {
        return netId;
    }

    public String getNetName()
    {
        return netName;
    }

    public Date getDateReceived()
    {
        return dateReceived == null ? null : new Date(dateReceived.getTime());
    }

    public String getRetailerId()
    {
        return retailerId;
    }

    public String getTransactionSource()
    {
        return transactionSource;
    }

    public String getUtilityType()
    {
        return utilityType;
    }

    public String getTransactionType()
    {
        return transactionType;
    }

    public Date getTransactionDate()
    {
        return transactionDate == null ? null : new Date(transactionDate.getTime());
    }

    public boolean isAnnotated()
    {
        return isAnnotated;
    }

    public String getTransactionStatus()
    {
        return transactionStatus;
    }

    public String getPaymentStatus()
    {
        return paymentStatus;
    }

    public String getGlId()
    {
        return glId;
    }
    
    public void setDateReceived(Date dateReceived)
    {
        this.dateReceived = dateReceived;
    }

    public void setTransactionDate(Date transactionDate)
    {
        this.transactionDate = transactionDate;
    }
    
    public void setTrxnId(String trxnId)
    {
        this.trxnId = trxnId;
    }

    public void setNetId(String netId)
    {
        this.netId = netId;
    }

    public void setNetName(String netName)
    {
        this.netName = netName;
    }

    public void setRetailerId(String retailerId)
    {
        this.retailerId = retailerId;
    }

    public void setTransactionSource(String transactionSource)
    {
        this.transactionSource = transactionSource;
    }

    public void setUtilityType(String utilityType)
    {
        this.utilityType = utilityType;
    }

    public void setTransactionType(String transactionType)
    {
        this.transactionType = transactionType;
    }

    public void setTransactionStatus(String transactionStatus)
    {
        this.transactionStatus = transactionStatus;
    }

    public void setPaymentStatus(String paymentStatus)
    {
        this.paymentStatus = paymentStatus;
    }

    public void setGlId(String glId)
    {
        this.glId = glId;
    }

    public void setAnnotated(boolean isAnnotated)
    {
        this.isAnnotated = isAnnotated;
    }
}
