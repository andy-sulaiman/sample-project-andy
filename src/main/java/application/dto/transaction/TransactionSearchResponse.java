package application.dto.transaction;

import java.util.List;

public class TransactionSearchResponse
{
    private int count;

    private List<Transaction> content;
    
    public TransactionSearchResponse()
    {}

    public TransactionSearchResponse(List<Transaction> content)
    {
        this.content = content;
        this.count = this.content.size();
    }

    public int getCount()
    {
        return count;
    }

    public List<Transaction> getContent()
    {
        return content;
    }
}
