package application.dto.pact;

public class State
{
    private String state;
    
    public State()
    {}
    
    public State(String state)
    {
        this.state = state;
    }
    
    public String getState()
    {
        return state;
    }
}
