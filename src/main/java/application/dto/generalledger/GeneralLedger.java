package application.dto.generalledger;

public class GeneralLedger
{
    private String gl_code;
    private String utility_type;
    private String gl_id;
    private String description;
    private String last_update_id;

    public GeneralLedger(String gl_code, String utility_type, String gl_id, String description, String last_update_id)
    {
        super();
        this.gl_code = gl_code;
        this.utility_type = utility_type;
        this.gl_id = gl_id;
        this.description = description;
        this.last_update_id = last_update_id;
    }

    public String getGl_code()
    {
        return gl_code;
    }

    public String getUtility_type()
    {
        return utility_type;
    }

    public String getGl_id()
    {
        return gl_id;
    }

    public String getDescription()
    {
        return description;
    }

    public String getLast_update_id()
    {
        return last_update_id;
    }
}
