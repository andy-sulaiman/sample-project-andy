package application.backend.repository.transaction;

import org.springframework.data.repository.CrudRepository;

import application.dto.transaction.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, String>
{

}
