package application.backend.pact.setup;

import org.springframework.web.client.RestTemplate;

import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionCreateResponse;

public class SetupState1 implements PactStateSetup
{
    private RestTemplate template;
    
    public SetupState1(RestTemplate template)
    {
        this.template = template;
    }
    
    @Override
    public void setup()
    {
        template.delete("http://localhost:8080/sample-web-app/transaction/clear");
        
        Transaction t = new Transaction();
        t.setTrxnId("10000");
        TransactionCreateResponse yy = template.postForEntity("http://localhost:8080/sample-web-app/transaction/create", t, 
                TransactionCreateResponse.class).getBody();
        
        System.out.println(yy.getMessage());
    }
}
