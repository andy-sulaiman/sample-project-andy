package application.backend.pact;

import java.util.HashMap;
import java.util.Map;

import application.backend.pact.setup.PactStateSetup;
import application.dto.pact.State;

public class StateMap implements StateProcessor
{
    private Map<String, PactStateSetup> setupProcessors;
    
    public StateMap()
    {
        setupProcessors = new HashMap<String, PactStateSetup>();
    }
    
    @Override
    public void process(State state)
    {
        setupProcessors.get(state.getState())
                        .setup();
    }
    
    public void addProcessor(String name, PactStateSetup processor)
    {
        setupProcessors.put(name, processor);
    }
}
