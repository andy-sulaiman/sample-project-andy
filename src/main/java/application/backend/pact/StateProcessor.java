package application.backend.pact;

import application.dto.pact.State;

public interface StateProcessor
{
    void process(State state);
}
