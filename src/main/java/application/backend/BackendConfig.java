package application.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import application.backend.pact.StateMap;
import application.backend.pact.StateProcessor;
import application.backend.pact.setup.SetupState1;
import application.backend.pact.setup.SetupState2;
import application.rest.RestConfig;

@Configuration
@Import(RestConfig.class)
public class BackendConfig
{
    @Autowired
    private RestTemplate template;
    
    @Bean
    public StateProcessor stateProcessor()
    {
        StateMap processor = new StateMap();
        
        processor.addProcessor("state1", new SetupState1(template));
        processor.addProcessor("state2", new SetupState2(template));
        
        return processor;
    }
}
