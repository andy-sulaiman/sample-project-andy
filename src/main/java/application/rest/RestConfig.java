package application.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import application.rest.transaction.HardcodedTransactionReturn;

@Configuration
public class RestConfig
{
    @Bean
    public HardcodedTransactionReturn hardcodedObjectReturner()
    {
        return new HardcodedTransactionReturn();
    }
    
    @Bean
    public RestTemplate restTemplate()
    {
        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
        converters.add(jsonConverter());
        
        return new RestTemplate(converters);
    }
    
    @Bean
    public MappingJackson2HttpMessageConverter jsonConverter()
    {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        
        List<MediaType> types = new ArrayList<MediaType>();
        types.add(MediaType.APPLICATION_JSON);
        converter.setSupportedMediaTypes(types);
        
        return converter;
    }
}
