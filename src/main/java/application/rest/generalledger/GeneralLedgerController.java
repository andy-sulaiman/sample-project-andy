package application.rest.generalledger;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import application.dto.generalledger.GeneralLedger;
import application.util.generalledger.GeneralLedgerUtil;

@RestController
@RequestMapping("/sample-web-app")
public class GeneralLedgerController
{
    @RequestMapping("/generalledger")
    public GeneralLedger getGeneralLedger()
    {
        return new GeneralLedger("ABCD-ABCD-BABA", "ELE", "4", "Gl Default", "3365");
    }

    @RequestMapping(value = "/generalledger/clone", method = RequestMethod.GET)
    public GeneralLedger cloneAndReturn(GeneralLedger orig)
    {
        return GeneralLedgerUtil.cloneLedger(orig);
    }
}
