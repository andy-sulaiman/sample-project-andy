package application.rest.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import application.dto.transaction.Transaction;

public class HardcodedTransactionReturn
{
    private List<Transaction> transactions;

    public HardcodedTransactionReturn()
    {
        transactions = new ArrayList<Transaction>();
        transactions.add(new Transaction("10000", "123", "NETWORK", new Date(), "1", "BTB", "ELE", "NOUS", new Date(),
                "DIPS", "UNPAID", "3", false));

        transactions.add(new Transaction("10001", "123", "NETWORK", new Date(), "1", "BTB", "ELE", "NOUS", new Date(),
                "DIPS", "UNPAID", "3", false));

        transactions.add(new Transaction("10001", "123", "NETWORK", new Date(), "1", "BTB", "ELE", "NOUS", new Date(),
                "DIPS", "UNPAID", "3", false));

        transactions.add(new Transaction("10002", "123", "NETWORK", new Date(), "1", "BTB", "ELE", "NOUS", new Date(),
                "DIPS", "UNPAID", "3", false));
    }

    public List<Transaction> getTransactionsById(String idToGet)
    {
        return transactions.stream().filter(t -> idToGet.equals(t.getTrxnId()))
                                    .collect(Collectors.toList());
    }

    public List<Transaction> getAllTransactions()
    {
        return transactions;
    }

    public void addTransaction(Transaction t)
    {
        transactions.add(t);
    }

    public void deleteTransaction(String id)
    {
        List<Transaction> toDelete = getTransactionsById(id);

        transactions.removeAll(toDelete);
    }
    
    public void clearTransactions()
    {
        transactions.clear();
    }
}
