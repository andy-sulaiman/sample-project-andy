package application.rest.transaction;

import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import application.backend.repository.transaction.TransactionRepository;
import application.dto.transaction.Transaction;
import application.dto.transaction.TransactionCreateResponse;
import application.dto.transaction.TransactionSearchResponse;

@RestController
@RequestMapping("/sample-web-app")
public class TransactionController
{
    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionController(TransactionRepository repository)
    {
        transactionRepository = repository;
    }
    
    @RequestMapping(value = "/transaction/clear", method = RequestMethod.DELETE)
    public void clearAllTransactions()
    {
        transactionRepository.deleteAll();
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.GET)
    public TransactionSearchResponse allTransactions()
    {        
        ArrayList<Transaction> returnList = new ArrayList<Transaction>();
        transactionRepository.findAll().forEach(returnList::add);
        
        return new TransactionSearchResponse(returnList);
    }

    @RequestMapping(value = "/transaction/{transId}", method = RequestMethod.GET)
    public TransactionSearchResponse transactionSearch(@PathVariable("transId") String id)
    {
        ArrayList<Transaction> returnList = new ArrayList<Transaction>();
        
        Transaction t = transactionRepository.findOne(id);
        
        if (t != null)
        {
            returnList.add(t);
        }
        
        return new TransactionSearchResponse(returnList);
    }

    @RequestMapping(value = "/transaction/create", method = RequestMethod.POST)
    public TransactionCreateResponse createTransaction(@RequestBody Transaction toAdd)
    {
        if (transactionRepository.exists(toAdd.getTrxnId()))
        {
            return new TransactionCreateResponse("Transaction already exists with id: " + toAdd.getTrxnId());
        }

        transactionRepository.save(toAdd);
        
        return new TransactionCreateResponse("Successfully added transaction with id: " + toAdd.getTrxnId());
    }

    @RequestMapping(value = "/transaction/delete/{transId}", method = RequestMethod.DELETE)
    public String deleteTransaction(@PathVariable("transId") String id)
    {
        transactionRepository.delete(id);

        return "Successfully deleted all transactions with id: " + id;
    }
    
    @ModelAttribute
    public void setAccessControlAllowOriginResponseHeader(HttpServletResponse response)
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
    }
}
