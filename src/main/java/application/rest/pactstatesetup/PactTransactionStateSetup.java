package application.rest.pactstatesetup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import application.backend.pact.StateProcessor;
import application.dto.pact.State;

@RestController
@RequestMapping("/sample-web-app/pact-setup")
public class PactTransactionStateSetup
{    
    @Autowired
    private StateProcessor processor;
    
    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public void setupTransactions(@RequestBody State stateMessage)
    {
        System.out.println("STATE: " + stateMessage.getState());
        
        processor.process(stateMessage);
    }
}
